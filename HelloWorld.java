import java.util.Scanner;

public class HelloWorld {
    public static void main(String[] args) {
        Scanner scn = new Scanner(System.in);
        System.out.print("Enter your name: ");
        String name = scn.nextLine();
        System.out.println("Hello, "+name);
        System.out.print("Enter your age: ");
        int age = scn.nextInt();
        System.out.println("You are "+age+ " years old.");
    }
}
